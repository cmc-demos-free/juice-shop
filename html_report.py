import sys
import json


def generate_html_report(data):
    html_report = """
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Vulnerability Report</title>
    </head>
    <body>
        <h1>Vulnerability Report</h1>
        <table border="1">
            <tr>
                <th>ID</th>
                <th>Category</th>
                <th>Name</th>
                <th>Description</th>
                <th>CVE</th>
                <th>Severity</th>
                <th>File</th>
                <th>Line</th>
            </tr>
            {rows}
        </table>
    </body>
    </html>
    """

    # Generate rows for vulnerabilities
    rows = ""
    for vulnerability in data["vulnerabilities"]:
        row = """
            <tr>
                <td>{id}</td>
                <td>{category}</td>
                <td>{name}</td>
                <td>{description}</td>
                <td>{cve}</td>
                <td>{severity}</td>
                <td>{file}</td>
                <td>{line}</td>
            </tr>
        """.format(
            id=vulnerability["id"],
            category=vulnerability["category"],
            name=vulnerability["name"],
            description=vulnerability["description"],
            cve=vulnerability["cve"],
            severity=vulnerability["severity"],
            file=vulnerability["location"]["file"],
            line=vulnerability["location"]["start_line"]
        )
        rows += row

    # Insert rows into the HTML template
    html_report = html_report.format(rows=rows)

    # Write HTML report to file
    with open("report.html", "w") as file:
        file.write(html_report)


if __name__ == "__main__":
    # Read the input from stdin
    report_content = sys.stdin.read()

    generate_html_report(json.loads(report_content))
